# Script to plot GSAS data in a nice format!
# Tested on Windows with Anaconda python, may require some modification for
# Linux or Mac.

import numpy as np
import subprocess
import pandas as pd
import os
import matplotlib.pyplot as plt
from matplotlib import gridspec
import argparse


__author__ = 'James Cumby'
__email__ = "james.cumby@ed.ac.uk"

# Usage: python gsas_plot.py EXPFILE
#
# Type python gsas_plot.py --help for more information


def get_hstdmp(expname, hist, GSASEXE="C:\GSAS\EXE"):
    """ Perform gsas hstdmp on expname for the specified histogram, and return the string output. """
    
    # Create input string
    inp = "{0}\nL\n{1}\n0\n".format(str(expname), str(hist))
    # Find location of hstdmp
    hstdmp = os.path.join(os.path.normpath(GSASEXE), 'hstdmp')
    
    # Method 1:
    # Run hstdmp as process, then pass in arguments after starting
    # !! This writes the results of HSTDMP to the .LST file
    #proc = subprocess.Popen(hstdmp, stdin = subprocess.PIPE, stdout=subprocess.PIPE)
    #out, err = proc.communicate(inp)
    
    # Method 2:
    # Write input to a file, then start hstdmp with a file redirection
    with open("HSTDMP.tmp", "w") as fileinp:
        fileinp.write(inp)
    
    proc = subprocess.Popen("{0} < HSTDMP.tmp".format(hstdmp), shell = True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    out, err = proc.communicate(None)
    os.remove("HSTDMP.tmp")
    
    out = out.decode()
    
    if out.find('FORTRAN error') > 0:
        raise ValueError('Error generating hstdump for histogram {} in {}.EXP'.format(hist, expname))
    
    return out
    
def hstdmp_to_pandas(hstdmp):
    """ Convert raw GSAS hstdmp.exe string to pandas DataFrame
    
    Returns
        data : pandas dataframe (units as data['X'].units property)
        peaks : list of reflection positions as (X, <phase numbers>)
    """
    
    # Convert output into list of lines
    lines = hstdmp.splitlines()
    
    record = []
    X = []
    Iobs = []
    Icalc = []
    bkg = []
    
    # Peaks holds positions for tickmarks
    # Format is (X, <phase string>)
    # where <phase string> contains numbers 1-9 corresponding to phase(s)
    peaks = []
    
    
    # Iterate over all lines, filtering out headers
    for l in lines:
        data = l.strip().split()
        if len(data) == 0:
            continue
        elif data[0] == 'RecNo':
            # Assume we're on a header line
            xunits = data[2]
        elif len(data) >= 9 and data[-2] == 'Page':
            # We're on a page separator
            continue
        elif data[0].isdigit():     # Faster than try/except, but only works for positive integers
            # Skip over excluded values
            if float(data[5]) > 0.0 and int(data[0]) >= 0:
                record.append(int(data[0]))
                X.append(float(data[2]))
                Iobs.append(float(data[3]))
                Icalc.append(float(data[4]))
                bkg.append(float(data[6]) + float(data[7]))
            
                if len(data[1]) > 1:
                    peaks.append((float(data[2]),[int(i) for i in data[1][1:]]))
        else:
            continue
        
    data = pd.DataFrame({'X':X,'Iobs':Iobs,'Icalc':Icalc,'Bkg':bkg}, index=record)
    
    data['X'].units = xunits
        
    return data, peaks

def peaks_as_dict(peaks):
    """ Take list of peak tuples (from hstdmp_to_pandas) and return dict of phases, with peak positions as list)"""
    
    uniquephs = set([j for i in peaks for j in i[1]])
    
    # Set up dict to hold phase peaks
    phases = {}
    for i in uniquephs:
        phases[i] = []
        
    for p in peaks:
        for l in p[1]:
            phases[l].append(p[0])
            
    return phases
            
def make_plot(data, 
              peaks=None, 
              xunits=None,
              yunits='Intensity (a.u.)',
              yscale='linear',
              ax=None, 
              diff=True,
              bkgnd = False,
              ticks=True,
              tickcols = None):
    """ Plot GSAS refinement from pandas DataFrame of Iobs, Icalc, Bkg and X. """
    
    mintick=0
    
    # Set up axes for plotting
    if ax is None:
        fig = plt.figure()
        gs = gridspec.GridSpec(2, 1, height_ratios=[4, 1], hspace=0) 
        ax = fig.add_subplot(gs[0])
        ax2 = fig.add_subplot(gs[1], sharex = ax)
        
    # Plot data, setting log y scale if needed.
    if yscale.lower() == 'linear' or yscale.lower() == 'lin':
        yscale='lin'
        data.plot(kind='scatter', x='X', y='Iobs', marker='x', color='k', ax=ax)
        data.plot(kind='line', x='X', y='Icalc', color='r', ax=ax)
    elif yscale.lower() == 'log' or yscale.lower() == 'logarithmic':
        yscale='log'
        data.plot(kind='scatter', x='X', y='Iobs', logy=True, marker='x', color='k', ax=ax)
        data.plot(kind='line', x='X', y='Icalc', logy=True, color='r', ax=ax)
    else:
        raise ValueError("Unknown yscale {}; valid options are 'linear' or 'log'".format(yscale))
    
    
    if ticks:
        # Calculate size of tickmarks based on full range of data to display
        if diff:
            # Maximum range of data + difference
            if yscale == 'lin':
                ticksize = ((data[['Iobs','Icalc']].max().max() + (data['Iobs'] - data['Icalc']).max())) * 0.02
            elif yscale == 'log':
                ticksize = np.log((data[['Iobs','Icalc']].max().max() + (data['Iobs'] - data['Icalc']).max())) * 0.02
        else:
            # Just use maximum of data to give tick size
            if yscale == 'lin':
                ticksize = ((data[['Iobs','Icalc']].max().max() )) * 0.02
            elif yscale == 'log':
                ticksize = np.log((data[['Iobs','Icalc']].max().max() )) * 0.02
            
    
        # Plot tick marks
        if peaks is None:
            raise ValueError('No tickmark values supplied')
        elif type(peaks) is list:
            phases = peaks_as_dict(peaks)
        elif type(peaks) is dict:
            phases = peaks.copy()
        
        if tickcols is None:
            tickcols = ['c','m','g','orange','brown','gold','maroon']
        else:
            if len(tickcols) < len(phases.keys()):
                raise ValueError('Not enough colors for the number of phases ({0})'.format(len(phases.keys())))
        
        if bkgnd:
            datmin = data[['Iobs','Icalc','Bkg']].min().min()*0.98
        else:
            datmin = data[['Iobs','Icalc']].min().min()*0.98

        for i, ph in enumerate(phases):
            ax.vlines(phases[ph], datmin-(i+1)*ticksize, datmin-(i+2)*ticksize, color=tickcols[i])
            
        mintick = datmin - (i+3)*ticksize
    
    if diff:
        # Calculate and plot difference
        diffdat = data['Iobs'] - data['Icalc']
        ax2.plot(data['X'], diffdat, color = 'b')
        #ax.plot(data['X'], diff)
        
        ax2.hlines(0, ax2.get_xlim()[0], ax2.get_xlim()[1], linestyle='-', color='lightgray')
    
    if bkgnd:
        # Plot background line
        if yscale == 'lin':
            data.plot(kind='line',x='X',y='Bkg',color='g', ax=ax)
        elif yscale == 'log':
            data.plot(kind='line',x='X',y='Bkg', logy=True, color='g', ax=ax)
    
    ax.legend_.remove()
    
    ax2.locator_params(nbins=3, axis='y')
    ax2.minorticks_on()
    
    # Set view limits
    ymin = mintick*0.975
    ymax = data[['Iobs','Icalc']].max().max()
    yrng = (ymin, ymax + 0.025*(ymax-ymin))
    ax.set_ylim(yrng)
    
    # Set titles
    if xunits is None:
        try:
            xunits = data['X'].units
        except AttributeError:
            xlabel = ''
        
        if xunits == 'Theta':
            xlabel = '$2\Theta (^{\circ})$'
        elif xunits == 'Time':
            xlabel = 'Time of Flight (ms)'
        elif xunits == 'keV':
            xlabel = 'Energy (keV)'
        else:
            xlabel = xunits
    else:
        xlabel = xunits
        
    ax2.set_xlabel(xlabel)
    
    ax.set_ylabel(yunits)
    
    return ax
    
def main(EXP, **kwargs):
    """ Read EXP file, and plot graph"""
    
    # Generate HSTDMP output
    rawhst = get_hstdmp(EXP,
                        hist=kwargs.get('hist'),
                        GSASEXE=kwargs.get('GSASEXE'),
                        )
                        
    # Convert data to pandas DataFrame and peak list
    hstfrm, peaks = hstdmp_to_pandas(rawhst)
    
    # Generate plot
    ax = make_plot(hstfrm,
                   peaks, 
                   yunits=kwargs.get('yunits', 'Intensity (a.u.)'),
                   yscale=kwargs.get('yscale','linear'), 
                   diff=kwargs.get('diff',True),
                   bkgnd = kwargs.get('bkgnd', False),
                   ticks = kwargs.get('ticks', True),                   
                  )
    
    return ax
            
            
if __name__ == '__main__':
    
    # Read in command line arguments
    parser = argparse.ArgumentParser(description='Plot GSAS refinement to a file by using the results of HSTDUMP.')
    
    parser.add_argument("EXPs", type=str, nargs='+', help="EXPGUI experiment file names to plot (include directory path if in a different directory)")
    parser.add_argument("--GSAS", type=str, default='"C:/GSAS/EXE"', help="Location of the GSAS executable files (default: 'C:/GSAS/EXE')")
    parser.add_argument("--histogram", default=1, type=int, help="Histogram number to plot (default: 1)")
    parser.add_argument("-y", "--y-label", type=str, default='Intensity (a.u.)', help="Y-axis label (default: 'Intensity (a.u.)')")
    parser.add_argument("-x", "--x-label", type=str, help="X-axis label (default: dependent on histogram)")
    parser.add_argument("-f", "--format", type=str, default='png', help="Output format for plot (default: png)")
    
    yscale = parser.add_mutually_exclusive_group(required=False)
    yscale.add_argument("--logy", action='store_true', help="Plot logarithmic y axis")
    yscale.add_argument("--liny", action='store_true', help="Plot linear y axis (default)")
    
    ticks = parser.add_mutually_exclusive_group(required=False)
    ticks.add_argument("--ticks", action='store_true', dest='ticks', help="Plot tickmarks (default)")
    ticks.add_argument("--no-ticks", action='store_false', dest='ticks', help="Plot without tickmarks")
    parser.set_defaults(ticks=True)

    diff = parser.add_mutually_exclusive_group(required=False)
    diff.add_argument("--diff", action='store_true', dest='diff', help="Plot difference curve (default)")
    diff.add_argument("--no-diff", action='store_false', dest='diff', help="Plot without difference curve")
    parser.set_defaults(diff=True)
    
    bkgnd = parser.add_mutually_exclusive_group(required=False)
    bkgnd.add_argument("--bg", action='store_true', dest='bkgnd', help="Plot background curve")
    bkgnd.add_argument("--no-bg", action='store_false', dest='bkgnd', help="Plot without background curve (default)")
    parser.set_defaults(bkgnd=False)
    
    args = parser.parse_args()
    
    # Handle path to GSAS correctly for Operating System.
    args.GSAS = os.path.normpath(args.GSAS)
    
    # Process yscale arguments
    if args.logy:
        yscale='logarithmic'
    else:
        yscale='linear'
        
    outformat = args.format.lower()
        
    for file in args.EXPs:
        # Remove any extension if present
        infile = os.path.splitext(os.path.normpath(file))[0]
        
        # Call the main processing function passing any options
        ax = main(infile,
                  GSASEXE=args.GSAS,
                  hist=args.histogram,
                  yscale=yscale,
                  yunits=args.y_label,
                  diff = args.diff,
                  ticks = args.ticks,
                  bkgnd = args.bkgnd
                  
                  
                 )
        
        ax.figure.savefig('{}_hist_{}.{}'.format(infile,args.histogram,outformat), bbox_inches='tight', dpi=600)
            
            
            
            
            
            
            
            
            
            