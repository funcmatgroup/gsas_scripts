GSAS Scripts
============

A repository of scripts written to interface with GSAS.

Available Scripts
-----------------

Script       | Purpose
-------------|-------------------------------------------------------------
gsas_plot.py | - Generate visual plots of GSAS results and write to a file


	


Authors
-------

James Cumby (james.cumby@ed.ac.uk)	